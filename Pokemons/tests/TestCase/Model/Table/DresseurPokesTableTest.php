<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DresseurPokesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DresseurPokesTable Test Case
 */
class DresseurPokesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DresseurPokesTable
     */
    public $DresseurPokes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DresseurPokes',
        'app.Dresseurs',
        'app.Pokes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DresseurPokes') ? [] : ['className' => DresseurPokesTable::class];
        $this->DresseurPokes = TableRegistry::getTableLocator()->get('DresseurPokes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DresseurPokes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
